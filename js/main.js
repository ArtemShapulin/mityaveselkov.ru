$(document).ready(function(){

      $('input, textarea').placeholder();


        var forSlaiderLength = $(".card-description_left-slaider-for-item").length;
        if (forSlaiderLength > 5){
               $('.card-description_left-slaider-for').slick({
                  slidesToShow: 5,
                  prevArrow: $('.for_slaider_prev'),
                  nextArrow: $('.for_slaider_next'),
                  // centerMode: true,
                  variableWidth: true,
                            responsive: [
            {
              breakpoint: 769,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            }

          ]
                })
                $('.card-description_left-slaider-for').css("margin-left", "25px");
        }
        else{
               $('.card-description_left-slaider-for').slick({
                slidesToShow: 5,
                arrows: false,
                variableWidth: true
              });
      }

    $('.card-description_left-slaider-main').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: false,
      arrows: false,
       responsive: [
            {
              breakpoint: 641,
              settings: {
                dots: true,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
              }
            }

          ]
    });

    $('.promo-left-block-slaider').slick({
        arrows: false,
        dots: true,
        infinite: true,
        autoplay: true,
        speed: 500
    });

     $('.reviews_slaider').slick({
  
        dots: false,
        prevArrow: $('.reviews_slaider_prev'),
        nextArrow: $('.reviews_slaider_next'),
        slidesToShow: 2,
        slidesToScroll: 2,
        autoplay: true,
        infinite: true,
        // speed: 500,
          responsive: [
            {
              breakpoint: 769,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              }
            },

             {
              breakpoint: 691,
              settings: {
                // variableWidth: true, 
                dots: true,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                autoplay: true,
                adaptiveHeight: true,
                speed: 500
              }
            }
          ]
    });



      if($(window).width() <= 1024) {
        $('.popular-products_wrapper').slick({
          arrows: true,
          prevArrow: $('.popular_slaider_prev'),
          nextArrow: $('.popular_slaider_next'),
          slidesToShow: 3,
          slidesToScroll: 1,
          // variableWidth: true, 
          responsive: [
                 {
              breakpoint: 769,
              settings: {
                 arrows: true,
                  prevArrow: $('.popular_slaider_prev'),
                  nextArrow: $('.popular_slaider_next'),
                  slidesToShow: 2,
                  slidesToScroll: 2,
                  // variableWidth: true, 
              }
            },
            {
              breakpoint: 691,
              settings: {
                dots: true,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                autoplay: true,
                speed: 500
              }
            }

          ]
        });


        $('.recommend-products_wrapper').slick({
          arrows: true,
          prevArrow: $('.recommend_slaider_prev'),
          nextArrow: $('.recommend_slaider_next'),
          slidesToShow: 3,
          slidesToScroll: 1,
          // variableWidth: true,
           responsive: [
                       {
              breakpoint: 769,
              settings: {
                 arrows: true,
                 prevArrow: $('.recommend_slaider_prev'),
                  nextArrow: $('.recommend_slaider_next'),
                  slidesToShow: 2,
                  slidesToScroll: 2,
                  // variableWidth: true, 
              }
            },
            {
              breakpoint: 691,
              settings: {
                dots: true,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                autoplay: true,
                speed: 500
              }
            }
          ]
        });

        $('.my-wishlist_wrapper').slick({
          arrows: true,
          prevArrow: $('.my-wishlist_slaider_prev'),
          nextArrow: $('.my-wishlist_slaider_next'),
          slidesToShow: 2,
          slidesToScroll: 2,
          variableWidth: true,
           responsive: [
            {
              breakpoint: 691,
              settings: {
                dots: true,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                autoplay: true,
                speed: 500
              }
            }
          ]
        });
      }

       if($(window).width() > 1024 && $(".recommend-products_wrapper").hasClass("slick-initialized")){
            $('.recommend-products_wrapper').slick('unslick');
          }
          if($(window).width() > 1024 && $(".popular-products_wrapper").hasClass("slick-initialized")){
            $('.popular-products_wrapper').slick('unslick');
          }
           if($(window).width() > 1024 && $(".my-wishlist_wrapper").hasClass("slick-initialized")){
            $('.my-wishlist_wrapper').slick('unslick');
          }


      $( window ).resize(function() {
          if($(window).width() > 1024 && $(".recommend-products_wrapper").hasClass("slick-initialized")){
            $('.recommend-products_wrapper').slick('unslick');
          }
          if($(window).width() > 1024 && $(".popular-products_wrapper").hasClass("slick-initialized")){
            $('.popular-products_wrapper').slick('unslick');
          }
           if($(window).width() > 1024 && $(".my-wishlist_wrapper").hasClass("slick-initialized")){
            $('.my-wishlist_wrapper').slick('unslick');
          }

       });

       var wishParam = [{arrows: true, prevArrow: $('.my-wishlist_slaider_prev'), nextArrow: $('.my-wishlist_slaider_next'), slidesToShow: 2, slidesToScroll: 2,variableWidth: true}];
       var slickParam = [{arrows: true, prevArrow: $('.recommend_slaider_prev'), nextArrow: $('.recommend_slaider_next'), slidesToShow: 2, slidesToScroll: 2,variableWidth: true}];



       $( window ).resize(function() {

            if($(window).width() <= 1024 && !($(".popular-products_wrapper").hasClass("slick-initialized"))) {
        $('.popular-products_wrapper').slick({
          arrows: true,
          prevArrow: $('.popular_slaider_prev'),
          nextArrow: $('.popular_slaider_next'),
          slidesToShow: 3,
          slidesToScroll: 1,
          // variableWidth: true, 
          responsive: [
                 {
              breakpoint: 769,
              settings: {
                 arrows: true,
                  prevArrow: $('.popular_slaider_prev'),
                  nextArrow: $('.popular_slaider_next'),
                  slidesToShow: 2,
                  slidesToScroll: 2,
                  // variableWidth: true, 
              }
            },
            {
              breakpoint: 691,
              settings: {
                dots: true,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                autoplay: true,
                speed: 500
              }
            }

          ]
        });
            }
       
 });


      $( window ).resize(function() {
        if($(".recommend-products_wrapper").hasClass("slick-initialized")){
          return false;
        }
        else{
              if($(window).width() <= 1024) {
                        $('.recommend-products_wrapper').slick({
                          arrows: true,
                          prevArrow: $('.recommend_slaider_prev'),
                          nextArrow: $('.recommend_slaider_next'),
                          slidesToShow: 3,
                          slidesToScroll: 1,
                          // variableWidth: true, 
                          responsive: [
                                    {
                              breakpoint: 769,
                              settings: {
                                 arrows: true,
                                   prevArrow: $('.recommend_slaider_prev'),
                                   nextArrow: $('.recommend_slaider_next'),
                                  slidesToShow: 2,
                                  slidesToScroll: 2,
                                  // variableWidth: true, 
                              }
                            },
                            {
                              breakpoint: 691,
                              settings: {
                                dots: true,
                                arrows: false,
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                infinite: true,
                                autoplay: true,
                                speed: 500
                              }
                            }

                          ]
                        });
              }
            }
        });

          $( window ).resize(function() {
                if($(".my-wishlist_wrapper").hasClass("slick-initialized")){
                  return false;
                }
        else{
            if($(window).width() <= 1024) {
                      $('.my-wishlist_wrapper').slick({
                        arrows: true,
                        prevArrow: $('.my-wishlist_slaider_prev'),
                        nextArrow: $('.my-wishlist_slaider_next'),
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        // variableWidth: true, 
                        responsive: [
                         {
                              breakpoint: 769,
                              settings: {
                                 arrows: true,
                                  prevArrow: $('.my-wishlist_slaider_prev'),
                                  nextArrow: $('.my-wishlist_slaider_next'),
                                  slidesToShow: 2,
                                  slidesToScroll: 2,
                                  // variableWidth: true, 
                              }
                            },
                          {
                            breakpoint: 691,
                            settings: {
                              dots: true,
                              arrows: false,
                              slidesToShow: 1,
                              slidesToScroll: 1,
                              infinite: true,
                              autoplay: true,
                              speed: 500
                            }
                          }

                        ]
                      });
            }
        }
 });




       $( window ).resize(function() { 
          if($(window).width() > 640) {
            $('.header-list-block_drop-menu').css('display', 'none');
          }
       });


  if($(window).width() <= 640) {
    $(".main-basket .advantages .advantages_item-block .main-basket_advantages_img1").attr("src", "img/bas-adv2-320.png");
    $(".main-basket .advantages .advantages_item-block .main-basket_advantages_img2").attr("src", "img/bas-adv3-320.png");
    $(".main-basket .advantages .advantages_item-block .main-basket_advantages_img3").attr("src", "img/bas-adv33-320.png");
    // $(".main-basket_product-item").siblings(".basket-join").wrapAll("<div class='basket-join_block'></div>");
  }
    $(".top-header-block_magnifier").click(function(){
      $(".top-header-block__search-btn a").css("visibility","hidden");
      $(".top-header-block__search-btn .top-header-block_search-input").css("display","inline-block");
    });
    $('.header-menu-button').click(function(){
     if($(window).width() <= 640) {
      	$('.header-menu-button').toggleClass('open');
      }
    });
 
   	$('.header-menu-button').click(function(){
    if($(window).width() <= 640) {
    	$('.header-menu-button-under').toggleClass('open');
    }
  	});

  	 $("body").on("mouseenter", ".popular-products__item", function() {
       if($(window).width() > 768) {
  	 	   $(this).children(".popular-products__item_fast-button").css("display", "block");
       }
  	 });

  	 $("body").on("mouseleave", ".popular-products__item", function() {
       if($(window).width() > 768) {
  	 	   $(this).children(".popular-products__item_fast-button").css("display", "none");
       }
  	 });

      $("body").on("mouseenter", ".my-wishlist__item", function() {
       if($(window).width() > 768) {
         $(this).children(".my-wishlist__item_fast-button").css("display", "block");
       }
     });

     $("body").on("mouseleave", ".my-wishlist__item", function() {
       if($(window).width() > 768) {
         $(this).children(".my-wishlist__item_fast-button").css("display", "none");
       }
     });

      $("body").on("mouseenter", ".recommend-products__item", function() {
        if($(window).width() > 768) {
          $(this).children(".recommend-products__item_fast-button").css("display", "block");
        }
     });

     $("body").on("mouseleave", ".recommend-products__item", function() {
      if($(window).width() > 768) {
        $(this).children(".recommend-products__item_fast-button").css("display", "none");
      }
     });

     $("body").on("click", ".header-bottom-block ul li", function() { 
      if ($(this).children(".header-bottom-block__item_menu").hasClass("open-menu-block")) {
          $(this).children(".header-bottom-block__item_menu").css("display", "none");
          $(this).children(".header-bottom-block__item_menu").removeClass('open-menu-block');
      }
      else{
        $(".header-bottom-block__item_menu").css("display", "none");
        $(".header-bottom-block__item_menu").removeClass('open-menu-block');
        $(this).children(".header-bottom-block__item_menu").addClass('open-menu-block');
        $(this).children(".header-bottom-block__item_menu").css("display", "block");
      }
     });

    


      $("body").on("click", ".main-basket_product_close-cross", function() {  
        $(this).parent(".main-basket_product-item").hide();
      });

     



      $("body").on("click", ".header-menu-button", function() {
        if($(window).width() <= 640) {
         if( $(".hidden-mobile-menu").hasClass('opened-hidden-menu')){
            // $('body').unbind('touchmove');
             $("body").css("overflow", "auto");
             $(".hidden-mobile-menu").css("left", "-258px");
             $(".hidden-mobile-menu").removeClass('opened-hidden-menu');
             $(".fixed-black").css("display", "none");
             $(".hidden-mobile-menu .header-menu-button").css("display", "none");
         }
         else{
          // $('body').bind('touchmove', function(e){e.preventDefault()});
          // $('.hidden-mobile-menu_content-wrapper').unbind('touchmove');
          var bodyWidth = $("body").height();
          console.log(bodyWidth);
           $(".hidden-mobile-menu .header-menu-button").css("display", "block");
           $(".hidden-mobile-menu").css("left", "0px");
           $(".hidden-mobile-menu").addClass('opened-hidden-menu');
           $(".fixed-black").css("backgroundColor", "rgb(13, 13, 17)").css("opacity", "0.902").css("position", "fixed").css("overflow", "hidden").css("width","100%").css("height", bodyWidth).css("zIndex","499").css("display", "block");
           $("body").css("overflow", "hidden");
        }
      }
      });

      $('.hidden-mobile-menu_content-wrapper').bind('mousewheel DOMMouseScroll', function(e) {
        var scrollTo = null;
        if (e.type == 'mousewheel') {
          scrollTo = (e.originalEvent.wheelDelta * -1);
        }
        else if (e.type == 'DOMMouseScroll') {
          scrollTo = 40 * e.originalEvent.detail;
        }
        if (scrollTo) {
          e.preventDefault();
          $(this).scrollTop(scrollTo + $(this).scrollTop());
        }
      });


            var modal = document.getElementById("myModal");
            var oneClickModal = document.getElementById("one-click-modal");
            var reviewModal = document.getElementById("review-modal");

            var reviewBtn = document.getElementById("send-review");
            var btn = document.getElementById("fast-watch");
            var oneClickBtn = document.getElementById("oneClickBtn");
            var getPresentsButton = document.getElementById("get-presents");

            var span = document.getElementsByClassName("close")[0];
            var closeReview = document.getElementById("closeReview");

            if (btn) {
            btn.onclick = function() {
                modal.style.display = "block";
            }}
             if (oneClickBtn) {
            oneClickBtn.onclick = function() {
                oneClickModal.style.display = "block";

            }}
             if (reviewBtn) {
            reviewBtn.onclick = function() {
                reviewModal.style.display = "block";
            }}

            if (getPresentsButton){
               getPresentsButton.onclick = function() {
                modal.style.display = "block";
            }}
            if (closeReview) {
              closeReview.onclick = function() {
                if (reviewModal) {
                  reviewModal.style.display = "none";
                }
              }
            }

            if (span) {
            span.onclick = function() {
              if(modal){
                  modal.style.display = "none";
              }
              if(oneClickModal){
                  oneClickModal.style.display = "none";
              }
              if(reviewModal){
                  reviewModal.style.display = "none";
              }
            }}

            // window.onclick = function(event) {
            //     if (event.target == modal) {
            //         modal.style.display = "none";
            //     }
            // }
            // window.onclick = function(event) {
            //     if (event.target == oneClickModal) {
            //         oneClickModal.style.display = "none";
            //     }
            // }
            //    window.onclick = function(event) {
            //     if (event.target == reviewModal) {
            //         reviewModal.style.display = "none";
            //     }
            // }


       $("body").on("click", ".header-list-block_mobile-menu", function() { 
        if($(window).width() <= 640) {
          if($(this).children(".header-list-block_drop-menu").hasClass("open-drop-menu")){
            $(this).children(".header-list-block_drop-menu").slideUp("slow");
            $(this).children(".header-list-block_drop-menu").removeClass("open-drop-menu");
            $(this).children("a").css("color", "#000");
          }
       
          else{
            $(".header-list-block_drop-menu").css("display", "none");
            $(".header-list-block_drop-menu").removeClass("open-drop-menu");
            $(".header-list-block_mobile-menu a").css("color", "#000"); 

            $(this).children(".header-list-block_drop-menu").addClass("open-drop-menu");
            $(this).children(".header-list-block_drop-menu").slideDown("slow");
            $(this).children("a").css("color", "rgb(86, 85, 165)");
          }
        }
       });


       jQuery(function($){
        $(document).mouseup(function (e){ 
          var div = $(".header-list-block_mobile-menu"); 
              if (!div.is(e.target) 
                && div.has(e.target).length === 0) { 
              div.children(".header-list-block_drop-menu").slideUp("slow");; 
              div.children("a").css("color", "#000");
              div.children(".header-list-block_drop-menu").removeClass("open-drop-menu");
              }
          });
        });



       $(window).on('wheel', function(e) {
        var delta = e.originalEvent.deltaY;
        if (delta > 0){ 
          $(".top-header-block__search-btn .top-header-block_search-input").css("display", "none");
          $(".header .top-header-block .top-header-block__search-btn a").css("visibility", "visible");
        }
      });


       $(".header .top-header-block").width($(".main-page-wrapper").width());
          $( window ).resize(function() { 
            $(".header .top-header-block").width($(".main-page-wrapper").width());
              if($(window).width() <= 768) {
                $(".modal").css("display","none");
              }
      });


      $("body").on("click", ".main-basket_product-item_quantity_up", function() {
        var quantityValue = $(this).closest(".main-basket_product-item_quantity").children(".main-basket_product-item_quantity_input").val();
        if (quantityValue >= 1) {
           quantityValue++;
           $(this).closest(".main-basket_product-item_quantity").children(".main-basket_product-item_quantity_input").val(quantityValue);
        }
        else{
          $(this).closest(".main-basket_product-item_quantity").children(".main-basket_product-item_quantity_input").val(1);
        }

      });

       $("body").on("click", ".main-basket_product-item_quantity_down", function() {
        var quantityValue = $(this).closest(".main-basket_product-item_quantity").children(".main-basket_product-item_quantity_input").val();
        if (quantityValue > 1) {
           quantityValue--;
           $(this).closest(".main-basket_product-item_quantity").children(".main-basket_product-item_quantity_input").val(quantityValue);
        }
        else{
          $(this).closest(".main-basket_product-item_quantity").children(".main-basket_product-item_quantity_input").val(1);
        }

      });



      $("body").on("click", ".business_modal_quantity_up", function() {
        var quantityValue = $(this).closest(".business_modal_item-input-text-block").children(".business_modal_item-input-text").val();
        if (quantityValue >= 1) {
           quantityValue++;
           $(this).closest(".business_modal_item-input-text-block").children(".business_modal_item-input-text").val(quantityValue);
        }
        else{
          $(this).closest(".business_modal_item-input-text-block").children(".business_modal_item-input-text").val(1);
        }

      });

       $("body").on("click", ".business_modal_quantity_down", function() {
        var quantityValue = $(this).closest(".business_modal_item-input-text-block").children(".business_modal_item-input-text").val();
        if (quantityValue > 1) {
           quantityValue--;
           $(this).closest(".business_modal_item-input-text-block").children(".business_modal_item-input-text").val(quantityValue);
        }
        else{
          $(this).closest(".business_modal_item-input-text-block").children(".business_modal_item-input-text").val(1);
        }
      });


            $("body").on("click", ".modal-content_card-item_quantity_up", function() {
        var quantityValue = $(this).closest(".modal-content_card_item_quantity").children(".modal-content_card_item_quantity input").val();
        if (quantityValue >= 1) {
           quantityValue++;
           $(this).closest(".modal-content_card_item_quantity").children(".modal-content_card_item_quantity input").val(quantityValue);
        }
        else{
          $(this).closest(".modal-content_card_item_quantity").children(".modal-content_card_item_quantity input").val(1);
        }

      });

       $("body").on("click", ".modal-content_card-item_quantity_down", function() {
        var quantityValue = $(this).closest(".modal-content_card_item_quantity").children(".modal-content_card_item_quantity input").val();
        if (quantityValue > 1) {
           quantityValue--;
           $(this).closest(".modal-content_card_item_quantity").children(".modal-content_card_item_quantity input").val(quantityValue);
        }
        else{
          $(this).closest(".modal-content_card_item_quantity").children(".modal-content_card_item_quantity input").val(1);
        }
      });





       



       $("body").on("keydown", ".main-basket_product-item_quantity_input", function(e) {
          if(e.key.length == 1 && e.key.match(/[^0-9]/)){
            e.preventDefault();
          };
      });

         $("body").on("keydown", ".business_modal_item-input-text", function(e) {
          if(e.key.length == 1 && e.key.match(/[^0-9]/)){
            e.preventDefault();
          };
      });




      $("body").on("mouseenter", ".business-catalog_item", function() {
          $(this).children(".business-catalog_item-block").children(".business-catalog_item-hidden-block").show();
      });
       $("body").on("mouseleave", ".business-catalog_item", function() {
          $(this).children(".business-catalog_item-block").children(".business-catalog_item-hidden-block").hide();
      });

       if($(window).width() <= 640) {
          $(".showroom input").val("записаться на посещение");
       }
       $( window ).resize(function() { 
          if($(window).width() <= 640) {
            $(".showroom input").val("записаться на посещение");
          }
       });




       // CARD SLAIDER
       // var currentSlide = $('.card-description_left-slaider-for').slick('slickCurrentSlide');

        // $(".slick-track .card-description_left-slaider-for-item").data("slick-index", 0).addClass("active-card-slide");

       $("body").on("click", ".slick-track .card-description_left-slaider-for-item", function() {
        var moveSlide = $(this).data("slick-index");
        var currentSlide = $('.card-description_left-slaider-for').slick('slickCurrentSlide');
        if (moveSlide !== currentSlide) {
          $('.card-description_left-slaider-main').slick('slickGoTo', moveSlide);
          $('.card-description_left-slaider-for').slick('slickGoTo', moveSlide);
        }
      });

       $("body").on("click", ".card-description_left-slaider-for .slick-next", function() {
          $('.card-description_left-slaider-main').slick('slickNext');
       });

        $("body").on("click", ".card-description_left-slaider-for .slick-prev", function() {
          $('.card-description_left-slaider-main').slick('slickPrev');
       });

       $('.card-description_left-slaider-main').on('swipe', function(event, slick, direction){
          if (direction == "right") {
            $('.card-description_left-slaider-for').slick('slickPrev');
          }
          if (direction == "left") {
            $('.card-description_left-slaider-for').slick('slickNext');
          }
        });

        $('.card-description_left-slaider-for').on('swipe', function(event, slick, direction){
          if (direction == "right") {
            $('.card-description_left-slaider-main').slick('slickPrev');
          }
          if (direction == "left") {
            $('.card-description_left-slaider-main').slick('slickNext');
          }
        });


        // $('[data-fancybox="slaider-gallery"]').fancybox({
        //     thumbs : {
        //                     autoStart : false
        //     },
        //    buttons : [
        //                     'slideShow',
        //                     'download',
        //                     'zoom',
        //                     'close'
        //               ]
        //   });
       // CARD SLAIDER




       //stars 
        var starsArray = $(".review-modal_stars-block img");
         $("body").on("click", ".review-modal_stars-block img", function() {
          for (var i = 0; i <= 4; i++) {
              starsArray[i].src = "img/rew-stars-grey.png";
            }
            var starIndex = $(this).index();
            for (var i = 0; i <= starIndex; i++) {
              starsArray[i].src = "img/rew-stars.png";
            }
          });
       //stars


       //price-picker
          var snapSlider = document.getElementById('price-picker');
          if(snapSlider){
              noUiSlider.create(snapSlider, {
                start: [ 500, 10000 ],
                // snap: true,
                connect: true,
                range: {
                  'min': 0,
                  'max': 15000
                }
          });

              var snapValues = [
                      document.getElementById('lower-price'),
                      document.getElementById('upper-price')
              ];

              snapSlider.noUiSlider.on('update', function( values, handle ) {
                  snapValues[handle].value = Math.floor(values[handle]);
              });

              var snapValuesText = [
                      document.getElementById('lower-price-text'),
                      document.getElementById('upper-price-text')
              ];

              snapSlider.noUiSlider.on('update', function( values, handle ) {
                  snapValuesText[handle].innerHTML = Math.floor(values[handle]);
              });
 
              document.getElementById('lower-price').addEventListener('change', function(){
                snapSlider.noUiSlider.set([this.value, null]);
              });

              document.getElementById('upper-price').addEventListener('change', function(){
                snapSlider.noUiSlider.set([null, this.value]);
              });

              }
              var datepicker = document.getElementById("datepicker");
              if(datepicker){

               $('#datepicker').datepicker($.extend({
                inline: true,
                changeYear: true,
                yearRange: '1940:2017',
                changeMonth: true,
            },
             $.datepicker.regional['ru']
           ));

               $.datepicker.regional['ru'] = {
                      closeText: 'Закрыть',
                      prevText: '&#x3c;Пред',
                      nextText: 'След&#x3e;',
                      currentText: 'Сегодня',
                      monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
                      'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                      monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
                      'Июл','Авг','Сен','Окт','Ноя','Дек'],
                      dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                      dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                      dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                      dateFormat: 'dd.mm.yy',
                      firstDay: 1,
                      isRTL: false
                      };
                      $.datepicker.setDefaults($.datepicker.regional['ru']);

                      $('input.datepicker').datepicker({
                      showOn: 'both',
                      buttonImageOnly: true
                      });
              }


               // $(".main-basket_product-item > .main-basket_product-item_cost, .main-basket_product-item > .main-basket_product-item_quantity").wrapAll("<div class='main-basket_product-join'></div>");
              
               $("body").on("click", ".hide_options", function() {
                if ( $(".catalog_find-options").hasClass("opened-options")) {
                    $(".catalog_find-options .catalog_find-options_sort").fadeIn("fast");
                    $(".catalog_find-options .catalog_find-options_category").fadeIn("fast");
                    $(".catalog_find-options .catalog_find-options_material").fadeIn("fast");
                    $(".catalog_find-options .catalog_find-options_colors-items .catalog_find-options_colors-block:nth-child(3), .catalog_find-options .catalog_find-options_colors-items .catalog_find-options_colors-block:nth-child(4)").fadeIn("fast");
                  if ($(window).width() <= 640) {
                     $(".catalog_find-options .catalog_find-options_colors").fadeIn("fast");
                  }
                  if ($(window).width() <= 480) {
                    $(".hide_options").text("Свернуть");
                  }
                  else{
                    $(".hide_options").text("Свернуть фильтр");
                  }
                    $(".catalog_find-options").removeClass("opened-options");
                  
                  }
                  else{
                    $(".catalog_find-options .catalog_find-options_sort").fadeOut("fast");
                    $(".catalog_find-options .catalog_find-options_category").fadeOut("fast");
                    $(".catalog_find-options .catalog_find-options_material").fadeOut("fast");
                    $(".catalog_find-options .catalog_find-options_colors-items .catalog_find-options_colors-block:nth-child(3), .catalog_find-options .catalog_find-options_colors-items .catalog_find-options_colors-block:nth-child(4)").fadeOut("fast");
                      if ($(window).width() <= 640) {
                     $(".catalog_find-options .catalog_find-options_colors").fadeOut("fast");
                  }
                  if ($(window).width() <= 480) {
                    $(".hide_options").text("Развернуть");
                  }
                  else{
                    $(".hide_options").text("Развернуть фильтр");
                  }
                  $(".catalog_find-options").addClass("opened-options");
                }
               });

                    if ($(window).width() <= 640) {
                     $(".catalog_find-options .catalog_find-options_colors").fadeOut("fast");
                    }
                    else{
                      $(".catalog_find-options .catalog_find-options_colors").fadeIn("fast");
                    }
                    // $( window ).resize(function() { 
                    //    if ($(window).width() <= 640) {
                    //       $(".catalog_find-options .catalog_find-options_colors").fadeOut("fast");
                    //     }
                    // else{
                    //    $(".catalog_find-options .catalog_find-options_colors").fadeIn("fast");
                    // }
                    // });

               if($(window).width() <= 768) {
                  $(".catalog_find-options .catalog_find-options_sort").fadeOut("fast");
                  $(".catalog_find-options .catalog_find-options_category").fadeOut("fast");
                  $(".catalog_find-options .catalog_find-options_material").fadeOut("fast");
                  $(".catalog_find-options .catalog_find-options_colors-items .catalog_find-options_colors-block:nth-child(3), .catalog_find-options .catalog_find-options_colors-items .catalog_find-options_colors-block:nth-child(4)").fadeOut("fast");
                  $(".hide_options").text("Развернуть фильтр");
                  $(".catalog_find-options").addClass("opened-options");
               }

               // $( window ).resize(function() { 
               //  if($(window).width() <= 768) {
               //    $(".catalog_find-options .catalog_find-options_sort").fadeOut("fast");
               //    $(".catalog_find-options .catalog_find-options_category").fadeOut("fast");
               //    $(".catalog_find-options .catalog_find-options_material").fadeOut("fast");
               //    $(".catalog_find-options .catalog_find-options_colors-items .catalog_find-options_colors-block:nth-child(3), .catalog_find-options .catalog_find-options_colors-items .catalog_find-options_colors-block:nth-child(4)").fadeOut("fast");
               //    $(".hide_options").text("Развернуть фильтр");
               //    $(".catalog_find-options").addClass("opened-options");
               //  }
               //  else{
               //    $(".catalog_find-options .catalog_find-options_sort").fadeIn("fast");
               //    $(".catalog_find-options .catalog_find-options_category").fadeIn("fast");
               //    $(".catalog_find-options .catalog_find-options_material").fadeIn("fast");
               //    $(".catalog_find-options .catalog_find-options_colors-items .catalog_find-options_colors-block:nth-child(3), .catalog_find-options .catalog_find-options_colors-items .catalog_find-options_colors-block:nth-child(4)").fadeIn("fast");
               //    $(".hide_options").text("Развернуть фильтр");
               //    $(".catalog_find-options").removeClass("opened-options");
               //  }
               // });

                if($(window).width() <= 480) {
                    $(".hide_options").text("Развернуть");
                    $(".catalog_find-options_reset_hidden_item:last-child a").text("Сбросить");
                }

                // $( window ).resize(function() { 
                //    if($(window).width() <= 480 && $("catalog_find-options").hasClass("opened-options")) {
                //       $(".hide_options").text("Развернуть");
                //       $(".catalog_find-options_reset_hidden_item:last-child a").text("Сбросить");
                //     }
                //     else{
                //       $(".hide_options").text("Развернуть фильтр");
                //       $(".catalog_find-options_reset_hidden_item:last-child a").text("Сбросить фильтры");
                //     }
                // });

                  $( window ).resize(function() { 
                    if ($(".catalog_find-options").hasClass("opened-options")){
                     if($(window).width() <= 480)  {
                        $(".hide_options").text("Развернуть");
                        $(".catalog_find-options_reset_hidden_item:last-child a").text("Сбросить");
                     }
                     else{
                      $(".hide_options").text("Развернуть фильтр");
                      $(".catalog_find-options_reset_hidden_item:last-child a").text("Сбросить фильтры");
                     }
                   }
                   else{
                        if($(window).width() <= 480 )  {
                          $(".hide_options").text("Свернуть");
                          $(".catalog_find-options_reset_hidden_item:last-child a").text("Сбросить");
                       }
                       else{
                        $(".hide_options").text("Свернуть фильтр");
                        $(".catalog_find-options_reset_hidden_item:last-child a").text("Сбросить фильтры");
                       }
                     }
  
                 
                  });


                if($(window).width() <= 768) {
                    $(".popular-products__item_fast-button").hide();
                }
                $( window ).resize(function() { 
                   if($(window).width() <= 768) {
                      $(".popular-products__item_fast-button").hide();
                    }
                });

              $('body').append(
                '<div style="position: fixed; z-index: 1005; bottom: 0; right: 0; background: #fff; border: solid 1px #828286; width: 200px;"><a href="javascript:void(0);" style="float: right;background:#ccc; color:#000; padding: 5px 10px; text-decoration: none; font-size: 16px" onclick="$(this).parent().hide()">Close X</a> <style> #pages { padding: 10px 20px 0 50px; font-size: 18px; } #pages a { text-decoration: none; } #pages li { margin: 5px 0; } </style> <ol id="pages"> <li><a href="index.html">Index</a></li> <li><a href="basket.html">Korzina</a></li> <li><a href="business.html">Podarok</a></li> <li><a href="card.html">Staranica tovara</a></li> <li><a href="catalog.html">catalog</a></li> <li><a href="formalization.html">Dostavka</a></li> <li><a href="formalized.html">Send</a></li> <li><a href="help.html">Help</a></li> <li><a href="opt.html">Opt</a></li><li><a href="help_item.html">Help item</a></li> <li><a href="lk.html">lk</a></li><li><a href="news.html">news</a></li><li><a href="news_item.html">news-item</a></li></ol> </div>');
            


              
















                if( $('.right-help-items')){
                //help
                 if($(window).width() < 1230 && $(window).width() > 738) {
                        $('.right-help-items').masonry({
                            // options
                            itemSelector: '.help-item',
                            columnWidth: 340,
                            gutter: 29,
                            fitWidth: true,
                            isFitWidth: true,
                            horizontalOrder: true
                        });
                    } else if ($(window).width() < 739){
                        $('.right-help-items').masonry({
                            // options
                            itemSelector: '.help-item',
                            columnWidth: '.help-item',
                            fitWidth: false,
                            isFitWidth: false,
                            horizontalOrder: true
                        });
                    } else {
                        $('.right-help-items').masonry({
                            // options
                            itemSelector: '.help-item',
                            columnWidth: 310,
                            gutter: 25,
                            fitWidth: true,
                            isFitWidth: true,
                            horizontalOrder: true
                        });
                    };

                    $(window).resize(function() {
                        if($(window).width() < 1230 && $(window).width() > 738) {
                            $('.right-help-items').masonry({
                                // options
                                itemSelector: '.help-item',
                                columnWidth: 340,
                                gutter: 29,
                                fitWidth: true,
                                isFitWidth: true,
                                horizontalOrder: true
                            });
                        } else if ($(window).width() < 739){
                            $('.right-help-items').masonry({
                                // options
                                itemSelector: '.help-item',
                                columnWidth: '.help-item',
                                fitWidth: false,
                                isFitWidth: false,
                                horizontalOrder: true
                            });
                             $('.right-help-items').css("width","100%");
                           
                        } 
                        else {
                            $('.right-help-items').masonry({
                                // options
                                itemSelector: '.help-item',
                                columnWidth: 310,
                                gutter: 25,
                                fitWidth: true,
                                 isFitWidth: true,
                                horizontalOrder: true

                            });
                        };
                });
                    }

    });

